package inf319ng.assocattr;

public class Pessoa {
	private String nome;
	private String sobrenome;
	private Contrato contratoTrabalho;

	public Pessoa() {
		nome = "";
		sobrenome = "";
		contratoTrabalho = null;
	}

	public Pessoa(String nome, String sobrenome) {
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.contratoTrabalho = null;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public Companhia getEmprego() {
		return (contratoTrabalho == null) ? null : contratoTrabalho.getEmpregador();
	}
	
	public double salario() {
		return contratoTrabalho.getSalario();
	}

	public void emprega(Contrato contrato) {
		this.contratoTrabalho = contrato;
	}

	public void demite() {
		this.contratoTrabalho = null;
	}
}
