INF319 - Projeto e Implementação Orientados a Objetos

Aluno: Guilherme Kayo Shida

Comentário:

- Existe uma relacionamento associativo utilizando a classe Contrato entre as classes Pessoa e Companhia;
- A classe Pessoa sabe onde qual a empresa que está relacionada através do atributo contratoTrabalho;
- A classe Companhia sabe quais são as pessoas da relação através do atributo contratosTrabalho;
- As classes conversam entre si através da associação Contrato.