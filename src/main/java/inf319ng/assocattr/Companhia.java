package inf319ng.assocattr;

import java.util.HashMap;
import java.util.Map;

public class Companhia {
	private String nome_companhia;
	private Map<Pessoa, Contrato> contratosTrabalho;

	public Companhia() {
		nome_companhia = "";
		contratosTrabalho = new HashMap<Pessoa, Contrato>();
	}

	public Companhia(String nome_companhia) {
		this.nome_companhia = nome_companhia;
		this.contratosTrabalho = new HashMap<Pessoa, Contrato>();
	}

	public String getNome() {
		return nome_companhia;
	}

	public void emprega(Pessoa pessoa, double salario) {
		Contrato novoContrato = new Contrato(this, pessoa, salario);
		this.contratosTrabalho.put(pessoa, novoContrato);
		pessoa.emprega(novoContrato);
	}

	public void demite(Pessoa pessoa) {
		this.contratosTrabalho.remove(pessoa);
		pessoa.demite();
	}

	public double custoTotal() {
		double custoTotal = 0.0;

		for (Contrato contrato : contratosTrabalho.values()) {
			custoTotal += contrato.getSalario();
		}

		return custoTotal;
	}
}
